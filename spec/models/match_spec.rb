require 'rails_helper'

RSpec.describe Match, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
  describe 'Validations' do
    describe 'Attributes' do
      it { should validate_presence_of(:score) }
    end

    # describe "Relationship" do
    #  it { should validate_presence_of(:tv_show) }
    # end
  end
  describe 'Relationships' do
    it { should belong_to(:game) }
    it { should belong_to(:champ) }
  end

  describe 'Attribute Existence' do
    it { should respond_to(:score) }
  end
end
