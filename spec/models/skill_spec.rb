require 'rails_helper'

RSpec.describe Skill, type: :model do
  describe 'Validations' do
    describe 'Attributes' do
      it { should validate_presence_of(:key) }
      it { should validate_presence_of(:image_url) }
    end

    # describe "Relationship" do
    #  it { should palidate_presence_of(:tv_show) }
    # end
  end

  describe 'Relationships' do
    it { should belong_to(:champ) }
  end

  describe 'Attribute Existence' do
    it { should respond_to(:name) }
    it { should respond_to(:key) }
    it { should respond_to(:image_url) }
  end
end
