require 'rails_helper'

RSpec.describe Game, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
  describe 'Validations' do
    describe 'Attributes' do
      it { should validate_presence_of(:name) }
    end

    # describe "Relationship" do
    #  it { should validate_presence_of(:tv_show) }
    # end
  end
  describe 'Relationships' do
    it { should have_many(:matches) }
  end

  describe 'Attribute Existence' do
    it { should respond_to(:name) }
    it { should respond_to(:description) }
  end
end
