require 'rails_helper'

RSpec.describe Champ, type: :model do
  describe 'Validations' do
    describe 'Attributes' do
      it { should validate_presence_of(:name) }
    end

    # describe "Relationship" do
    #  it { should validate_presence_of(:tv_show) }
    # end
  end
  describe 'Relationships' do
    it { should have_many(:skills) }
  end

  describe 'Attribute Existence' do
    it { should respond_to(:name) }
    it { should respond_to(:description) }
    it { should respond_to(:image_url) }
  end
end
