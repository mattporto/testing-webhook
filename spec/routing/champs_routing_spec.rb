require "rails_helper"

RSpec.describe ChampsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/champs").to route_to("champs#index")
    end

    it "routes to #new" do
      expect(:get => "/champs/new").to route_to("champs#new")
    end

    it "routes to #show" do
      expect(:get => "/champs/1").to route_to("champs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/champs/1/edit").to route_to("champs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/champs").to route_to("champs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/champs/1").to route_to("champs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/champs/1").to route_to("champs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/champs/1").to route_to("champs#destroy", :id => "1")
    end

  end
end
