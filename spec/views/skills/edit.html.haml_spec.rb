require 'rails_helper'

RSpec.describe "skills/edit", type: :view do
  before(:each) do
    @skill = assign(:skill, Skill.create!(
      :name => "MyString",
      :key => "MyString",
      :image_url => "MyString",
      :champ => nil
    ))
  end

  it "renders the edit skill form" do
    render

    assert_select "form[action=?][method=?]", skill_path(@skill), "post" do

      assert_select "input#skill_name[name=?]", "skill[name]"

      assert_select "input#skill_key[name=?]", "skill[key]"

      assert_select "input#skill_image_url[name=?]", "skill[image_url]"

      assert_select "input#skill_champ_id[name=?]", "skill[champ_id]"
    end
  end
end
