require 'rails_helper'

RSpec.describe 'matches/index', type: :view do
  before(:each) do
    assign(:matches, [
             Match.create!(
               score: 2
             ),
             Match.create!(
               score: 2
             )
           ])
  end

  it 'renders a list of matches' do
    render
    assert_select 'tr>td', text: 2.to_s, count: 2
  end
end
