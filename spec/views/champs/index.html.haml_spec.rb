require 'rails_helper'

RSpec.describe "champs/index", type: :view do
  before(:each) do
    assign(:champs, [
      Champ.create!(
        :name => "Name",
        :description => "Description",
        :image_url => "Image Url"
      ),
      Champ.create!(
        :name => "Name",
        :description => "Description",
        :image_url => "Image Url"
      )
    ])
  end

  it "renders a list of champs" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Image Url".to_s, :count => 2
  end
end
