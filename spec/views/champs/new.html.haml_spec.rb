require 'rails_helper'

RSpec.describe "champs/new", type: :view do
  before(:each) do
    assign(:champ, Champ.new(
      :name => "MyString",
      :description => "MyString",
      :image_url => "MyString"
    ))
  end

  it "renders new champ form" do
    render

    assert_select "form[action=?][method=?]", champs_path, "post" do

      assert_select "input#champ_name[name=?]", "champ[name]"

      assert_select "input#champ_description[name=?]", "champ[description]"

      assert_select "input#champ_image_url[name=?]", "champ[image_url]"
    end
  end
end
