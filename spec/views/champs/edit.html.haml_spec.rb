require 'rails_helper'

RSpec.describe "champs/edit", type: :view do
  before(:each) do
    @champ = assign(:champ, Champ.create!(
      :name => "MyString",
      :description => "MyString",
      :image_url => "MyString"
    ))
  end

  it "renders the edit champ form" do
    render

    assert_select "form[action=?][method=?]", champ_path(@champ), "post" do

      assert_select "input#champ_name[name=?]", "champ[name]"

      assert_select "input#champ_description[name=?]", "champ[description]"

      assert_select "input#champ_image_url[name=?]", "champ[image_url]"
    end
  end
end
