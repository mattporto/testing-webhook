class AddSkillToChamp < ActiveRecord::Migration[5.0]
  def change
    create_table :skills do |t|
      t.integer :champ_id
      t.string :key
      t.string :description
      t.string :image_url
    end
  end
end
