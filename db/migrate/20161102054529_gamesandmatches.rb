class Gamesandmatches < ActiveRecord::Migration[5.0]
  def change
    change_table :matches do |t|
      t.integer :champ_id
      t.integer :game_id
    end
  end
end
