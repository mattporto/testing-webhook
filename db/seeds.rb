
# User
u = User.create(email: 'admin@lolzero.com.br', password: 'xumbregs')
u.add_role(:admin)
u.save

# Games

# Champions
champ = Champ.create(name: 'vayne', image_url: 'http://www.mobafire.com/images/champion/icon/vayne.png')

# Skills
Skill.create(key: 'q', image_url: 'http://www.mobafire.com/league-of-legends/ability/tumble-385', champ: champ)
Skill.create(key: 'w', image_url: 'http://www.mobafire.com/league-of-legends/ability/silver-bolts-386', champ: champ)
Skill.create(key: 'e', image_url: 'http://www.mobafire.com/images/ability/vayne-condemn.png', champ: champ)
Skill.create(key: 'r', image_url: 'http://www.mobafire.com/images/ability/vayne-final-hour.png', champ: champ)
