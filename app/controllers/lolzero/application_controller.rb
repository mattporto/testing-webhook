class Lolzero::ApplicationController < ActionController::Base
  layout 'lolzero'

  include Pundit
  protect_from_forgery with: :exception
end
