class Lolzero::SkillGuessController < Lolzero::ApplicationController
  def index
    @champ = Lolzero::SkillGuess.random_champ
    @q = get_image_url('q')
    @w = get_image_url('w')
    @e = get_image_url('e')
    @r = get_image_url('r')
    @question_image = '//st.depositphotos.com/1002881/2899/i/170/depositphotos_28990789-Question-stone.jpg'
  end

  private

  def get_image_url(key)
    missing_url = 'http://muskoka411.com/start/wp-content/uploads/2016/04/missing.png'
    (@champ && @champ.skills.where(key: key).first.try(:image_url)) || missing_url
  end
end
