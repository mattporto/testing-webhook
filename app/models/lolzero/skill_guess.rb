class Lolzero::SkillGuess
  class << self
    def random_champ
      Champ.all.sample
    end

    def create_from_jsons
      json=JSON.parse(open('http://ddragon.leagueoflegends.com/cdn/6.21.1/data/en_US/champion.json').read )
      json = json['data']
      json.keys.each do |k|
        Champ.create(
          name: json[k]['name'],
          image_url: "http://ddragon.leagueoflegends.com/cdn/5.2.1/img/champion/#{json[k]['name']}.png"
        )
      end
    end
  end
end
