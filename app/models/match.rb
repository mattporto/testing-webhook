class Match < ApplicationRecord
  validates :score, presence: true

  belongs_to :game
  belongs_to :champ
end
