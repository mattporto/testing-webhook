json.extract! skill, :id, :name, :key, :image_url, :champ_id, :created_at, :updated_at
json.url skill_url(skill, format: :json)