json.extract! champ, :id, :name, :description, :image_url, :created_at, :updated_at
json.url champ_url(champ, format: :json)