json.extract! match, :id, :score, :created_at, :updated_at
json.url match_url(match, format: :json)
