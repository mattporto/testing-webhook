Rails.application.routes.draw do
  root 'lolzero/skill_guess#index'
  devise_for :users
  resources :games
  resources :matches
  resources :skills
  resources :champs
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
